// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"

#include "PickUp.generated.h"

UCLASS()
class FP312_API APickUp : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickUp();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
	USceneComponent* PickUpRoot;

	// Create static mesh for pick up
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* MyPickUpMesh;
	
	UPROPERTY(EditAnywhere)
	UShapeComponent* PickUpSphere;

	UFUNCTION(BlueprintCallable, Category = "Quest Items")
	void OnPlayerEnterPickUpSphere(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);


};
