// Fill out your copyright notice in the Description page of Project Settings.

#include "FP312.h"
#include "MyCharacter.h"
#include "MyHUD.h"

void AMyHUD::DrawHUD()
{
	Super::DrawHUD();
	
	// Draw HUD Border Rectangle
	DrawRect(FLinearColor::Gray, HUDX, HUDY, HUDW + (HUDBW * 2.f), HUDH + (HUDBW * 2.f));
	
	// Draw HUD Background rectangle
	DrawRect(FLinearColor::Black, HUDX + HUDBW, HUDY + HUDBW, HUDW, HUDH);

	AMyCharacter* CollectedItems = Cast<AMyCharacter>(GetOwningPawn());
	AMyCharacter* CameraNum = Cast<AMyCharacter>(GetOwningPawn());

	// Create string to display collected quest items and retrieve the current number of collected Items
	QuestItems = FString::Printf(TEXT("Quest Items Collected: %d of %d"), (CollectedItems->GetQuestItemCount()), TotalItems);

	// The specifics of where and how to display the text on the screen
	DrawText(QuestItems, FColor::White, HUDX + HUDBW +10, HUDY + HUDBW + 10, hudFont);


	// 2nd hud for camera indication
	// Draw HUD Border Rectangle
	DrawRect(FLinearColor::Gray, HUDX, HUDY + 50, HUDW + (HUDBW * 2.f), HUDH + (HUDBW * 2.f));

	// Draw HUD Background rectangle
	DrawRect(FLinearColor::Black, HUDX + HUDBW, HUDY + 50 + HUDBW, HUDW, HUDH);

	// Create string to retrieve and display current camera
	CameraPos = FString::Printf(TEXT("Camera Number %d"), (CameraNum->GetCameraNum()));

	// The specifics of where and how to display the text on the screen
	DrawText(CameraPos, FColor::White, HUDX + HUDBW + 10, HUDY + 50 + HUDBW + 10, hudFont);
}

