// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"
#include "MyHUD.generated.h"

/**
 * 
 */
UCLASS()
class FP312_API AMyHUD : public AHUD
{
	GENERATED_BODY()
	
public:
	// Initialize font to the HUD
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUDFont")
		UFont* hudFont;
	
	// Implement the DrawHUD() funtion to display
	virtual void DrawHUD() override;
	
	// HUD Width without Border
	float HUDW = 200;
	
	//HUD Height without Border
	float HUDH = 35;

	// Border around the HUD
	float HUDBW = 2;

	// Total HUD distance from left of screen
	float HUDX = 10;

	// Total HUD distance from top of screen
	float HUDY = 10;

	// String for DrawText
	FString QuestItems = "";
	FString CameraPos = "";

	// Variables to keep track of Quest Items for the HUD
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest Items")
	int CollectedItems = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest Items")
	int TotalItems = 4;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Position")
	int CameraNum = 3;

};
