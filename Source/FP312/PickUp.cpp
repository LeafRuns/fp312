// Fill out your copyright notice in the Description page of Project Settings.

#include "FP312.h"
#include "MyHUD.h"
#include "PickUp.h"


// Sets default values
APickUp::APickUp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	PickUpRoot = CreateAbstractDefaultSubobject<USceneComponent>(TEXT("PickUpRoot"));
	RootComponent = PickUpRoot;

	// Create and Attach mesh to PickUpRoot
	MyPickUpMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MyPickUpMesh"));
	MyPickUpMesh->AttachToComponent(PickUpRoot, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

	// Create a sphere collision for overlap event
	PickUpSphere = CreateDefaultSubobject<USphereComponent>(TEXT("PickUpSphere"));
	PickUpSphere->SetWorldScale3D(FVector(5.0f));
	PickUpSphere->bGenerateOverlapEvents = true;
	PickUpSphere->OnComponentBeginOverlap.AddDynamic(this, &APickUp::OnPlayerEnterPickUpSphere);
	PickUpSphere->AttachToComponent(PickUpRoot, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
}

// Called when the game starts or when spawned
void APickUp::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APickUp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APickUp::OnPlayerEnterPickUpSphere(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	Destroy();
}

