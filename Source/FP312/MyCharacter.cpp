// Fill out your copyright notice in the Description page of Project Settings.

#include "FP312.h"
#include "MyCharacter.h"

// Sets default values
AMyCharacter::AMyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	TriggerCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Player Trigger"));
	TriggerCapsule->InitCapsuleSize(55.5f, 96.f); //SetWorldScale3D(FVector(10.0f));
	TriggerCapsule->SetCollisionProfileName(TEXT("Trigger"));
	TriggerCapsule->SetupAttachment(RootComponent);

	TriggerCapsule->OnComponentBeginOverlap.AddDynamic(this, &AMyCharacter::OnOverlapBegin);

}

// Called when the game starts or when spawned
void AMyCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(InputComponent);
	InputComponent->BindAxis("ForwardBack", this, &AMyCharacter::ForwardBack);
	InputComponent->BindAxis("Strafe", this, &AMyCharacter::Strafe);
	InputComponent->BindAxis("Turn", this, &AMyCharacter::AddControllerYawInput);

	InputComponent->BindAction("Sprint", IE_Pressed, this, &AMyCharacter::StartSprint);
	InputComponent->BindAction("Sprint", IE_Released, this, &AMyCharacter::EndSprint);
	

	InputComponent->BindAction("CameraOne", IE_Pressed, this, &AMyCharacter::ChangeActiveCamera<0>);
	InputComponent->BindAction("CameraTwo", IE_Pressed, this, &AMyCharacter::ChangeActiveCamera<1>);
	InputComponent->BindAction("FollowCamera", IE_Pressed, this, &AMyCharacter::ChangeActiveCamera<2>);

}

void AMyCharacter::ForwardBack(float Value)
{
	if (Controller&&Value)
	{
		if (bIsSprinting)
			Value *= 2;
		FVector Forward = GetActorForwardVector();
		AddMovementInput(Forward, Value / 2);
	}
}


// Called to move character Penman forward or Backward. Sets speed of movement based of wither bIsSprinting is true or false
void AMyCharacter::Strafe(float Value)
{
	if (Controller&&Value)
	{
		if (bIsSprinting)
			Value *= 2;
		FVector Right = GetActorRightVector();
		AddMovementInput(Right, Value / 2);
	}
}

// Sets bIsSprinting to true
void AMyCharacter::StartSprint()
{
	bIsSprinting = true;
}

// Sets bIsSprinting to false
void AMyCharacter::EndSprint()
{
	bIsSprinting = false;
}
void AMyCharacter::SetActiveCamera(int CamNum)
{
	for (TActorIterator<ACameraControl>ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		if (CamNum == 0)
		{
			ActorItr->Camera1();
			CameraNum = 1;
			break;
		}

		else if (CamNum == 1)
		{
			ActorItr->Camera2();
			CameraNum = 2;
			break;
		}

		else if (CamNum == 2)
		{
			ActorItr->FCamera();
			CameraNum = 3;
			break;
		}
	}
}

// When the Character capsule overlaps an item, increments the number of items collected for scoring on the character
void AMyCharacter::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if(OtherActor->IsA(ACharacter::StaticClass()))
	{ 
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("You have been Caught!"));
		caught = true;
		UGameplayStatics::SetGamePaused(this, true);
	}
	else
	{ 
		CollectedQuestItems += 1;
		if(CollectedQuestItems == 4)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("You win!"));
			UGameplayStatics::SetGamePaused(this, true);
		}
	}
}

// Allows CollectedQuestItem variable to be called by other classes
int AMyCharacter::GetQuestItemCount()
{
	return CollectedQuestItems;
}

int AMyCharacter::GetCameraNum()
{
	return CameraNum;
}
