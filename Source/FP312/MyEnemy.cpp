// Fill out your copyright notice in the Description page of Project Settings.

#include "FP312.h"
#include "MyEnemy.h"
#include "MyEnemyAIController.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Perception/PawnSensingComponent.h"

// Sets default values
AMyEnemy::AMyEnemy()
{
 	
	//initialize AI's senses
	PawnSensingComp = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComp"));
	PawnSensingComp->SetPeripheralVisionAngle(90.0f);
}

// Called when the game starts or when spawned
void AMyEnemy::BeginPlay()
{
	Super::BeginPlay();

	if (PawnSensingComp)
	{
		PawnSensingComp->OnSeePawn.AddDynamic(this, &AMyEnemy::OnPlayerCaught);
	}
	
}

// Called to bind functionality to input
void AMyEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AMyEnemy::OnPlayerCaught(APawn* Pawn)
{
	//get reference to player controller
	AMyEnemyAIController* AIController = Cast<AMyEnemyAIController>(GetController());

	if (AIController)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("You have been Found!"));
		AIController->SetPlayerCaught(Pawn);
		
	}

}