// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "MyEnemyAIController.generated.h"

/**
 * 
 */
UCLASS()
class FP312_API AMyEnemyAIController : public AAIController
{
	GENERATED_BODY()

		//Blackboard component
		UPROPERTY(transient)
		class UBehaviorTreeComponent* BehaviorComp;

		UPROPERTY(transient)
		class UBlackboardComponent* BlackboardComp;

		// Blackboard Keys
		UPROPERTY(EditDefaultsOnly, Category = AI)
			FName LocationToGoKey;

		UPROPERTY(EditDefaultsOnly, Category = AI)
			FName PlayerKey;

		// Patrol point array
		TArray<AActor*> PatrolPoints;

		virtual void Possess(APawn* Pawn) override;

	
public:

	AMyEnemyAIController();

	void SetPlayerCaught(APawn* Pawn);

	int32 CurrentPatrolPoint = 0;

	// Getter functions
	FORCEINLINE UBlackboardComponent* GetBlackboardComp() const { return BlackboardComp; }
	FORCEINLINE TArray<AActor*> GetPatrolPoints() const { return PatrolPoints; }
};
