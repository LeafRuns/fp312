// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine.h"
#include "CameraControl.h"
#include "GameFramework/Character.h"
#include "MyCharacter.generated.h"

UCLASS()
class FP312_API AMyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMyCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Move Forward and Backward
	void ForwardBack(float Value);

	// Move Right and Left
	void Strafe(float Value);

	// Start Player sprinting
	void StartSprint();

	// Stop Player sprinting
	void EndSprint();

	bool bIsSprinting = false;

	// Variables to keep track of Quest Items
	UPROPERTY(VisibleAnywhere, Category = "Quest Items")
		int CollectedQuestItems = 0;

	UFUNCTION(BlueprintCallable, Category = "Quest Items")
		int GetQuestItemCount();

	// Variables to keep track of Camera Position
	UPROPERTY(VisibleAnywhere, Category = "Camera Position")
		int CameraNum = 3;

	UFUNCTION(BlueprintCallable, Category = "Camera Position")
		int GetCameraNum();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game Over")
		bool caught = false;

	//Setup handling of the assignment of the inputs from key bindings to SetActiveCamera function
	template <int Index>
	void ChangeActiveCamera() { SetActiveCamera(Index); }
	void SetActiveCamera(int CamNum);

	UPROPERTY(VisibleAnywhere, Category = "Player Trigger")
	class UCapsuleComponent* TriggerCapsule;

	// Overlap stuff
	UFUNCTION(BlueprintCallable, Category = "Quest Items")
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
	
