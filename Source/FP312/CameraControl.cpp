// Fill out your copyright notice in the Description page of Project Settings.

#include "FP312.h"
#include "Kismet/GameplayStatics.h"
#include "CameraControl.h"


// Sets default values
ACameraControl::ACameraControl()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACameraControl::BeginPlay()
{
	Super::BeginPlay();

	OurPlayerController = UGameplayStatics::GetPlayerController(this, 0);

}

// Called every frame
void ACameraControl::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//
void ACameraControl::Camera1()
{
	if (OurPlayerController)
	{
		OurPlayerController->SetViewTarget(CameraOne);
	}
}

void ACameraControl::Camera2()
{
	if (OurPlayerController)
	{
		OurPlayerController->SetViewTarget(CameraTwo);
	}
}

void ACameraControl::FCamera()
{
	if (OurPlayerController)
	{
		OurPlayerController->SetViewTarget(FollowCamera);
	}
}