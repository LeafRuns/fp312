// Fill out your copyright notice in the Description page of Project Settings.

#include "FP312.h"
#include "BTSelectPatrolPoint.h"
#include "MyEnemyTargetPoint.h"
#include "MyEnemyAIController.h"
#include "BehaviorTree/BlackboardComponent.h"

EBTNodeResult::Type UBTSelectPatrolPoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8 * NodeMemory)
{
	// Get AI
	AMyEnemyAIController* AICon = Cast<AMyEnemyAIController>(OwnerComp.GetAIOwner());
	
	if (AICon)
	{
		// Get Blackboard component
		UBlackboardComponent* BlackboardComp = AICon->GetBlackboardComp();
		
		// define AI Patrol point
		AMyEnemyTargetPoint* CurrentPoint = Cast<AMyEnemyTargetPoint>(BlackboardComp->GetValueAsObject("LocationToGo"));

		//Array of available points
		TArray<AActor*> AvailablePatrolPoints = AICon->GetPatrolPoints();

		AMyEnemyTargetPoint* NextPatrolPoint = nullptr;

		// Check to see if AI has reached end of array if not move to next point
		if (AICon->CurrentPatrolPoint != AvailablePatrolPoints.Num() - 1)
		{
			NextPatrolPoint = Cast<AMyEnemyTargetPoint>(AvailablePatrolPoints[++AICon->CurrentPatrolPoint]);
		}
		else// if there is no more patrol points return to the first one
		{
			NextPatrolPoint = Cast<AMyEnemyTargetPoint>(AvailablePatrolPoints[0]);
			AICon->CurrentPatrolPoint = 0;
		}

		//Go to next point
		BlackboardComp->SetValueAsObject("LocationToGo", NextPatrolPoint);

		return EBTNodeResult::Succeeded;
	}

	return EBTNodeResult::Failed;
}
