// Fill out your copyright notice in the Description page of Project Settings.

#include "FP312.h"
#include "MyEnemyAIController.h"
#include "MyEnemy.h"
#include "MyEnemyTargetPoint.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"

AMyEnemyAIController::AMyEnemyAIController()
{
	// initialixe blackboard and behavior tree
	BehaviorComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorComp"));
	BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComp"));

	// initialize blackboard keys
	PlayerKey = "Target";
	LocationToGoKey = "LocationtoGo";

	CurrentPatrolPoint = 0;
}

void AMyEnemyAIController::Possess(APawn * Pawn)
{
	Super::Possess(Pawn);

	// get refference to character
	AMyEnemy* MyEBot = Cast<AMyEnemy>(Pawn);
	
	if (MyEBot)
	{
		if (MyEBot->BehaviorTree->BlackboardAsset)
		{
			BlackboardComp->InitializeBlackboard(*(MyEBot->BehaviorTree->BlackboardAsset));
		}

		// Populate patrol point array
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), AMyEnemyTargetPoint::StaticClass(), PatrolPoints);

		BehaviorComp->StartTree(*MyEBot->BehaviorTree);
	}
}


//set player caught
void AMyEnemyAIController::SetPlayerCaught(APawn* Pawn)
{
	if (BlackboardComp)
	{
		BlackboardComp->SetValueAsObject(PlayerKey, Pawn);
	}
}